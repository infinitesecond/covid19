import { useEffect } from 'react'
import Router from 'next/router'
import * as Fathom from 'fathom-client'
import '../app/styles/base.scss'

// Record a pageview when route changes
Router.events.on('routeChangeComplete', () => {
  Fathom.trackPageview()
})

export default function MyApp ({ Component, pageProps }) {
  // Initialize Fathom when the app loads
  useEffect(() => {
    if (process.env.NODE_ENV === 'production') {
      Fathom.load()
      Fathom.setSiteId('EUZKKWJF')
      Fathom.trackPageview()
    }
  }, [])

  return <Component {...pageProps} />
}
