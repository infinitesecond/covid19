import { memo } from 'react'
import { useQuery } from 'react-query'

import { getTotalCasesByCounties } from '../data/index'

import LineGraph from './lineGraph'

function TotalCases ({ selectedCounties }) {
  const { data, status } = useQuery(['total_cases', { ids: selectedCounties }], getTotalCasesByCounties)
  const shouldShow = status === 'success' && selectedCounties.length > 0

  return shouldShow && (
    <div className="chart transition duration-500 ease-in-out pt-5 rounded overflow-hidden shadow-lg bg-white" style={{ height: '700px' }}>
      <h2 className="text-black text-xl font-bold pb-2 pl-5">Total Confirmed Cases</h2>
      <LineGraph data={data} />
    </div>
  )
}

const MemoizedTotalCases = memo(TotalCases)

export default MemoizedTotalCases
